# Changes
Here is the list of the changes to the internet experience enabled by the last release of minimal (0.4.2). All changes are made client-side by the user and have no effects on the websites themselves.

## Youtube
<details>
<summary>Remove some useless suggestion around the video description</summary>

> « There should be no suggestions that have no obvious link to the main content.
If a message is not strongly linked to the main content, it is an advertisement either for suggested content or for the platform, or both, and should be signaled as such. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/youtube.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css) at [line 1](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css#L1).

</details>
<details>
<summary>Remove trending and premium links</summary>

> « Any curation process should be made in the user's interest.
A platform curation process should only help the user find interesting content. This process, if not properly disclosed, signals that a conflict of interest might be at play. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/youtube.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css) at [line 10](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css#L10).

</details>
<details>
<summary>Simplify youtube logo to make it more discrete</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/youtube.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css) at [line 18](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css#L18).

</details>
<details>
<summary>Remove useless youtube appdrawer</summary>

> « Shortcuts must link to related content or actions that are hard to access.
There is no need for shortcuts that invite to use services already easily accessible and/or not obviously linked to the main content. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/youtube.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css) at [line 27](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css#L27).

</details>
<details>
<summary>Remove end screen video suggestions and eventual playlist sidebar (replaced by the playlist button on the top left corner of the video)</summary>

> « Any curation process should be made in the user's interest.
A platform curation process should only help the user find interesting content. This process, if not properly disclosed, signals that a conflict of interest might be at play. »
> « The main content of the page should stay the only focus
The user should see the main content first and foremost when landing on a page. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/youtube.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css) at [line 32](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css#L32).

</details>
<details>
<summary>Desaturate thumbnails</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/youtube.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css) at [line 51](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css#L51).

</details>
<details>
<summary>Use a neutral color for the bottom video progress bar</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/youtube.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css) at [line 57](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css#L57).

</details>
<details>
<summary>Use a neutral color for the subscribe button</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »
> « Additional content must be relevant.  
Displaying additional unasked information can be used to change the behavior of the user. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/youtube.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css) at [line 62](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css#L62).

</details>
<details>
<summary>Remove the colored border of the join button</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »
> « Additional content must be relevant.  
Displaying additional unasked information can be used to change the behavior of the user. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/youtube.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css) at [line 73](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css#L73).

</details>
<details>
<summary>Remove bottom right video branding</summary>

> « Shortcuts must link to related content or actions that are hard to access.
There is no need for shortcuts that invite to use services already easily accessible and/or not obviously linked to the main content. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/youtube.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css) at [line 78](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css#L78).

</details>
<details>
<summary>Remove the theater button</summary>

> « A page must only have one key purpose.
Other functions must be accessed by an action of the user, not forced onto them. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/youtube.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css) at [line 83](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css#L83).

</details>
<details>
<summary>Remove the minify button</summary>

> « A page must only have one key purpose.
Other functions must be accessed by an action of the user, not forced onto them. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/youtube.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css) at [line 88](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css#L88).

</details>
<details>
<summary>Remove the "next video" button when the video is not in a playlist</summary>

> « There should be no suggestions that have no obvious link to the main content.
If a message is not strongly linked to the main content, it is an advertisement either for suggested content or for the platform, or both, and should be signaled as such. »
> « Additional content must be relevant.  
Displaying additional unasked information can be used to change the behavior of the user. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/youtube.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css) at [line 93](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css#L93).

</details>
<details>
<summary>Remove all video suggestions on homepage</summary>

> « Additional content must be relevant.  
Displaying additional unasked information can be used to change the behavior of the user. »
> « Any curation process should be made in the user's interest.
A platform curation process should only help the user find interesting content. This process, if not properly disclosed, signals that a conflict of interest might be at play. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/youtube.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css) at [line 101](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css#L101).

</details>
<details>
<summary>Change color of activated navbar buttons to black</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/youtube.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css) at [line 106](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/youtube.css#L106).

</details>
<details>
<summary>Force theater mode</summary>

> « The main content of the page should stay the only focus
The user should see the main content first and foremost when landing on a page. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [scripts/youtube.js](https://gitlab.com/aupya/minimal/tree/0.4.2/scripts/youtube.js) at [line 1](https://gitlab.com/aupya/minimal/tree/0.4.2/scripts/youtube.js#L1).

</details>
<details>
<summary>Replace the subscription list from the side menu by a link to the subscription manager</summary>

> « Shortcuts must link to related content or actions that are hard to access.
There is no need for shortcuts that invite to use services already easily accessible and/or not obviously linked to the main content. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [scripts/youtube.js](https://gitlab.com/aupya/minimal/tree/0.4.2/scripts/youtube.js) at [line 11](https://gitlab.com/aupya/minimal/tree/0.4.2/scripts/youtube.js#L11).

</details>
<details>
<summary>Hide the live chat from the user and replace it with its button</summary>

> « A page must only have one key purpose.
Other functions must be accessed by an action of the user, not forced onto them. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [scripts/youtube.js](https://gitlab.com/aupya/minimal/tree/0.4.2/scripts/youtube.js) at [line 42](https://gitlab.com/aupya/minimal/tree/0.4.2/scripts/youtube.js#L42).

</details>
<details>
<summary>Remove the trending page</summary>

> « Any curation process should be made in the user's interest.
A platform curation process should only help the user find interesting content. This process, if not properly disclosed, signals that a conflict of interest might be at play. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [scripts/background/youtube.js](https://gitlab.com/aupya/minimal/tree/0.4.2/scripts/background/youtube.js) at [line 2](https://gitlab.com/aupya/minimal/tree/0.4.2/scripts/background/youtube.js#L2).

</details>
<details>
<summary>Remove autoplay "feature"</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [scripts/background/youtube.js](https://gitlab.com/aupya/minimal/tree/0.4.2/scripts/background/youtube.js) at [line 14](https://gitlab.com/aupya/minimal/tree/0.4.2/scripts/background/youtube.js#L14).

</details>

## Facebook
<details>
<summary>Remove messaging sidebar and popups from all pages</summary>

> « A page must only have one key purpose.
Other functions must be accessed by an action of the user, not forced onto them. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/facebook.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/facebook.css) at [line 1](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/facebook.css#L1).

</details>
<details>
<summary>Remove stories block</summary>

> « The main content of the page should stay the only focus
The user should see the main content first and foremost when landing on a page. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/facebook.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/facebook.css) at [line 15](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/facebook.css#L15).

</details>
<details>
<summary>Change the navbar color to match the background color</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/facebook.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/facebook.css) at [line 22](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/facebook.css#L22).

</details>
<details>
<summary>Change the navbar color to a neutral one on mobile</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/facebook.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/facebook.css) at [line 38](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/facebook.css#L38).

</details>
<details>
<summary>Remove unnecessary links in left column</summary>

> « Shortcuts must link to related content or actions that are hard to access.
There is no need for shortcuts that invite to use services already easily accessible and/or not obviously linked to the main content. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/facebook.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/facebook.css) at [line 51](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/facebook.css#L51).

</details>
<details>
<summary>Make legal links as visible as other elements in the right colum</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/facebook.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/facebook.css) at [line 62](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/facebook.css#L62).

</details>
<details>
<summary>Hide the textarea to create a post until you click on "create a post"</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/facebook.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/facebook.css) at [line 69](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/facebook.css#L69).

</details>
<details>
<summary>Remove messenger button on mobile browsers</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/facebook.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/facebook.css) at [line 77](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/facebook.css#L77).

</details>
<details>
<summary>Link the messaging icon in the navbar to the message page.</summary>

> « A page must only have one key purpose.
Other functions must be accessed by an action of the user, not forced onto them. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [scripts/facebook.js](https://gitlab.com/aupya/minimal/tree/0.4.2/scripts/facebook.js) at [line 1](https://gitlab.com/aupya/minimal/tree/0.4.2/scripts/facebook.js#L1).

</details>
<details>
<summary>Remove the install messenger mobile page</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [scripts/background/facebook.js](https://gitlab.com/aupya/minimal/tree/0.4.2/scripts/background/facebook.js) at [line 2](https://gitlab.com/aupya/minimal/tree/0.4.2/scripts/background/facebook.js#L2).

</details>

## Twitter
<details>
<summary>Twitter logo is more discrete</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/twitter.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/twitter.css) at [line 1](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/twitter.css#L1).

</details>
<details>
<summary>Hide the Home tab's "new content" bubble</summary>

> « Additional content must be relevant.  
Displaying additional unasked information can be used to change the behavior of the user. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/twitter.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/twitter.css) at [line 7](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/twitter.css#L7).

</details>

## Google
<details>
<summary>Make google logo more discrete</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/google.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/google.css) at [line 1](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/google.css#L1).

</details>
<details>
<summary>Use text instead of cards on mobile</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/google.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/google.css) at [line 7](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/google.css#L7).

</details>
<details>
<summary>Hide floating searchbar</summary>

> « The main content of the page should stay the only focus
The user should see the main content first and foremost when landing on a page. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/google.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/google.css) at [line 21](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/google.css#L21).

</details>

## Stackoverflow
<details>
<summary>Hide "featured" meta pannel, newsletter prompt, teams prompt</summary>

> « Shortcuts must link to related content or actions that are hard to access.
There is no need for shortcuts that invite to use services already easily accessible and/or not obviously linked to the main content. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/stackoverflow.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/stackoverflow.css) at [line 1](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/stackoverflow.css#L1).

</details>
<details>
<summary>Remove hot network questions</summary>

> « There should be no suggestions that have no obvious link to the main content.
If a message is not strongly linked to the main content, it is an advertisement either for suggested content or for the platform, or both, and should be signaled as such. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/stackoverflow.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/stackoverflow.css) at [line 7](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/stackoverflow.css#L7).

</details>
<details>
<summary>Hide badges and reputation from topbar</summary>

> « Additional content must be relevant.  
Displaying additional unasked information can be used to change the behavior of the user. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/stackoverflow.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/stackoverflow.css) at [line 11](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/stackoverflow.css#L11).

</details>
<details>
<summary>Remove left content border</summary>

> No existing rule from the [manifesto](MANIFESTO.md) has been defined for this change.
> ___ 
> This change is implemented in [styles/stackoverflow.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/stackoverflow.css) at [line 16](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/stackoverflow.css#L16).

</details>
<details>
<summary>Make top bar's colors fit the background color</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/stackoverflow.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/stackoverflow.css) at [line 21](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/stackoverflow.css#L21).

</details>
<details>
<summary>Make the logo more discrete</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/stackoverflow.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/stackoverflow.css) at [line 33](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/stackoverflow.css#L33).

</details>
<details>
<summary>Make related questions colors less bright</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/stackoverflow.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/stackoverflow.css) at [line 39](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/stackoverflow.css#L39).

</details>
<details>
<summary>Make navigation colors neutral</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/stackoverflow.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/stackoverflow.css) at [line 52](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/stackoverflow.css#L52).

</details>

## Amazon
<details>
<summary>Remove main page overwhelming fullpage suggestion</summary>

> « The main content of the page should stay the only focus
The user should see the main content first and foremost when landing on a page. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/amazon.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css) at [line 1](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css#L1).

</details>
<details>
<summary>Remove product page arbitrary suggestions</summary>

> « There should be no suggestions that have no obvious link to the main content.
If a message is not strongly linked to the main content, it is an advertisement either for suggested content or for the platform, or both, and should be signaled as such. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/amazon.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css) at [line 10](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css#L10).

</details>
<details>
<summary>Remove other futile suggestions</summary>

> « There should be no suggestions that have no obvious link to the main content.
If a message is not strongly linked to the main content, it is an advertisement either for suggested content or for the platform, or both, and should be signaled as such. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/amazon.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css) at [line 15](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css#L15).

</details>
<details>
<summary>Use neutral colors for the navbar</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/amazon.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css) at [line 21](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css#L21).

</details>
<details>
<summary>Make logo more discrete</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/amazon.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css) at [line 61](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css#L61).

</details>
<details>
<summary>Make button colors neutral</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/amazon.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css) at [line 70](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css#L70).

</details>
<details>
<summary>Desaturate orange titles</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/amazon.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css) at [line 75](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css#L75).

</details>
<details>
<summary>Remove the buy now button on desktop</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/amazon.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css) at [line 80](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css#L80).

</details>
<details>
<summary>Remove the "Pay in x installments free of charge" prompt</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/amazon.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css) at [line 84](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css#L84).

</details>
<details>
<summary>Remove availability information from the the description of the product</summary>

> « Additional content must be relevant.  
Displaying additional unasked information can be used to change the behavior of the user. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/amazon.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css) at [line 88](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css#L88).

</details>
<details>
<summary>Remove sharing buttons from the product page</summary>

> « Shortcuts must link to related content or actions that are hard to access.
There is no need for shortcuts that invite to use services already easily accessible and/or not obviously linked to the main content. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/amazon.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css) at [line 92](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css#L92).

</details>
<details>
<summary>Remove recently viewed products from a product page</summary>

> « A page must only have one key purpose.
Other functions must be accessed by an action of the user, not forced onto them. »
> « There should be no suggestions that have no obvious link to the main content.
If a message is not strongly linked to the main content, it is an advertisement either for suggested content or for the platform, or both, and should be signaled as such. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/amazon.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css) at [line 97](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css#L97).

</details>
<details>
<summary>Remove the cart right sidebar</summary>

> « A page must only have one key purpose.
Other functions must be accessed by an action of the user, not forced onto them. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/amazon.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css) at [line 107](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/amazon.css#L107).

</details>

## Yahoo

## Netflix
<details>
<summary>Desaturate logo</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/netflix.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/netflix.css) at [line 1](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/netflix.css#L1).

</details>
<details>
<summary>Use neutral color for progression bars</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/netflix.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/netflix.css) at [line 6](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/netflix.css#L6).

</details>
<details>
<summary>Remove full width suggestions</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/netflix.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/netflix.css) at [line 22](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/netflix.css#L22).

</details>
<details>
<summary>Make homepage tiles more sober</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/netflix.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/netflix.css) at [line 27](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/netflix.css#L27).

</details>
<details>
<summary>Hide homepage tiles video autoplay</summary>

> « Additional content must be relevant.  
Displaying additional unasked information can be used to change the behavior of the user. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/netflix.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/netflix.css) at [line 36](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/netflix.css#L36).

</details>
<details>
<summary>Remove homepage tiles audio and video autoplay</summary>

> « Additional content must be relevant.  
Displaying additional unasked information can be used to change the behavior of the user. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [scripts/netflix.js](https://gitlab.com/aupya/minimal/tree/0.4.2/scripts/netflix.js) at [line 1](https://gitlab.com/aupya/minimal/tree/0.4.2/scripts/netflix.js#L1).

</details>

## Reddit
<details>
<summary>Hide the total karma, on top right corner under username</summary>

> « Additional content must be relevant.  
Displaying additional unasked information can be used to change the behavior of the user. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/reddit.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/reddit.css) at [line 1](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/reddit.css#L1).

</details>
<details>
<summary>Hide the right panel until hover</summary>

> « There should be no suggestions that have no obvious link to the main content.
If a message is not strongly linked to the main content, it is an advertisement either for suggested content or for the platform, or both, and should be signaled as such. »
> « Any curation process should be made in the user's interest.
A platform curation process should only help the user find interesting content. This process, if not properly disclosed, signals that a conflict of interest might be at play. »
> *From the [manifesto](MANIFESTO.md).*
> ___ 
> This change is implemented in [styles/reddit.css](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/reddit.css) at [line 14](https://gitlab.com/aupya/minimal/tree/0.4.2/styles/reddit.css#L14).

</details>

